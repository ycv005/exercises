"""
Imagine the following pipeline class with the skeleton code below

A pipeline is a tube into which objects are put, and from which the4 are dispatched to various places.

The steps to send data this are as follows:
􏰀. Call the publish(objects) method of a pipeline, with our objects as arguments – as many times as you need.
􏰁. Once done, call the flush() method to indicate that you’re done publishing for now

if you never call the flush() method, your objects will never be sent down the tube (garbage collected);
or at least not completely.
It can be very easy for a programmer to forget about a flush() call, which breaks the program without giving an4
clues as to what might be wrong

Complete the _publisher() method below to ensure that your user can never make this mistake
"""


class Pipeline(object):

    def _publish(self, objects):
        # Imagine publication code here
        pass

    def _flush(self):
        # Imagine flushing code here
        pass

    def _publisher(self):
        pass

"""
Imagine you have a sample code below where you need to log imp stuff in one ore more method(s)
"""

import logging


class BadException(Exception):
    pass


class CriticalFunctionality(object):
    def __init__(self):
        ...

        self.logger = logging.getLogger(
            '.'.join([
                self.__module__,
                self.__class__.__name__
            ])
        )

    def carry_out_the_functionality(self):
        try:
            ...
        except BadException:
            self.logger.error('Oops something went wrong')


# I also wanted to log something here so I copy-pasted the logger from above

class CriticalBusinessFunctionality(object):

    def __init__(self):
        self.logger = logging.getLogger(
            '.'.join([
                self.__module__,
                self.__class__.__name__
            ])
        )
        self.logger.debug('Initialized CriticalBusinessFunctionality')

# Imagine I have 10+ such classes which need similar logging in different methods what would I do?
# You're not allowed to create a logging library for the purpose of this question.
# Find a feature in python (other than decorators) that can help you solve this



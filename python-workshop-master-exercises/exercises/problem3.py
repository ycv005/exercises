"""
Does this code scale for M = 1000000?
Re-write the code to make it scalable
It should scale both in Python 2 and Python 3
"""


def fetch_squares(max_root):
    squares = []
    for n in range(max_root):
        squares.append(n ** 2)
    return squares


MAX = 5

for square in fetch_squares(MAX):
    print(square)

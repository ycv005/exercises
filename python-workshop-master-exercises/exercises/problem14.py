"""
Consider an online store with these discount rules:

Customers with 1,000 or more fidelity points get a global 5% discount per order
A 10% discount is applied to each line item with 20 or more units in the same order.
Orders with at least 10 distinct items get a 7% global discount.

For brevity, let’s assume that only one discount may be applied to an order.

Implement an Order class with *pluggable* discount strategies

Test cases below -

joe = Customer('John Doe', 0)
ann = Customer('Ann Smith', 1100)
cart = [LineItem('banana', 4, .5),
         LineItem('apple', 10, 1.5),
         LineItem('watermellon', 5, 5.0)]
Order(joe, cart, FidelityPromo())
<Order total: 42.00 due: 42.00>

Order(ann, cart, FidelityPromo())
<Order total: 42.00 due: 39.90>

banana_cart = [LineItem('banana', 30, .5),
                LineItem('apple', 10, 1.5)]
Order(joe, banana_cart, BulkItemPromo())
<Order total: 30.00 due: 28.50>

long_order = [LineItem(str(item_code), 1, 1.0)
               for item_code in range(10)]

Order(joe, long_order, LargeOrderPromo())
<Order total: 10.00 due: 9.30>

Order(joe, cart, LargeOrderPromo())
<Order total: 42.00 due: 42.00>
"""





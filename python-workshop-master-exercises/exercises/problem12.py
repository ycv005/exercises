"""
Implement a generic function that prints the values of a container in a formatted way based on the container type

e.g.

# >>> fprint('hello')
# (str) hello

# >>> fprint(21)
# (int) 21

# >>> fprint(3.14159)
# (float) 3.14159

# >>> fprint([2, 3, 5, 7, 11])
# list -> index : value
# ---------------------
# 0 : (int) 2
# 1 : (int) 3
# 2 : (int) 5
# 3 : (int) 7
# 4 : (int) 11

# >>> fprint({2, 3, 5, 7, 11})
# set -> index : value
# --------------------
# 0 : (int) 2
# 1 : (int) 3
# 2 : (int) 5
# 3 : (int) 7
# 4 : (int) 11

# >>> fprint((13, 17, 23, 29, 31))
# tuple -> index : value
# ----------------------
# 0 : (int) 13
# 1 : (int) 17
# 2 : (int) 23
# 3 : (int) 29
# 4 : (int) 31

# >>> fprint({'name': 'John Doe', 'age': 32, 'location': 'New York'})
# dict -> key : value
# -------------------
# (str) name: (str) John Doe
# (str) age: (int) 32
# (str) location: (str) New York

"""

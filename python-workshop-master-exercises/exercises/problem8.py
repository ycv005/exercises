# given this dictionary

mcase = {'a': 10, 'b': 34, 'A': 7, 'Z': 3}

# write a one-liner to get this output

mcase_frequency = {'a': 17, 'z': 3, 'b': 34}

# given this list

input = [1, 1, 2]

# Write a one-liner to compute the unique squares of all integers

output = {1, 4}

# Write a one-liner to find out all the numbers between 0 to n which are multiples of 3
# n is bounded between 30 to 3000000 so your code should be pretty efficient

# Given the for loop below which finds multiples of 2

for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, 'equals', x, '*', n/x)
            break

# Change the above code to also print the prime numbers between 2 and 10
# Hint - the answer involves a 2 line change only
# Expected output
# 2 is a prime number
# 3 is a prime number
# 4 equals 2 * 2.0
# 5 is a prime number
# 6 equals 2 * 3.0
# 7 is a prime number
# 8 equals 2 * 4.0
# 9 equals 3 * 3.0
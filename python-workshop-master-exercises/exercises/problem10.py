"""
How do I make the below code more readable + maintainable + re-usable?
Also fix any other anti-patterns that you see in this code
"""

import requests


def make_get_request(url):
    response = requests.get(url)
    return response


def make_post_request(url, data):
    response = requests.post(url, data)
    return response


def make_put_request(url, data):
    response = requests.put(url, data)
    return response


def make_patch_request(url, data):
    response = requests.patch(url, data)
    return response


def make_delete_request(url):
    response = requests.delete(url)
    return response


def handle_request(request_object):
    response = None

    if request_object['verb'] is 'GET':
        response = make_get_request(request_object['url'])
    elif request_object['verb'] is 'POST':
        response = make_post_request(request_object['url'], request_object['data'])
    elif request_object['verb'] is 'PUT':
        response = make_put_request(request_object['url'], request_object['data'])
    elif request_object['verb'] is 'PATCH':
        response = make_patch_request(request_object['url'], request_object['data'])
    elif request_object['verb'] is 'DELETE':
        response = make_delete_request(request_object['url'])

    return response

"""
Simplify the code below
Target - Result should be in 4 lines
"""

words = 'a quick brown fox jumped'.split()
count = {}

for word in words:
    if word in count:
        count[word] += 1
    else:
        count[word] = 1

print(count)

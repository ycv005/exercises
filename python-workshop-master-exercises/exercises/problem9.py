# Imagine we have this code to generate fibonnaci numbers

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


# Measure its time using these statements

import timeit

timeit.timeit('fibonacci(35)', globals=globals(), number=1)

# Output - 3.9369294929999796 seconds

# Now I did some python magic and the execution time for fibonnaci(35) now is
# Output - 2.9145000098651508e-05 seconds

# What magic did I do?

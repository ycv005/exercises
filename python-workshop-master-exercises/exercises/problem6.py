# Imagine you are implementing an image processing library, creating classes to read the image from storage.
# So you create a base ImageReader class, and several derived types:

import abc


class ImageReader(metaclass=abc.ABCMeta):
    def __init__(self, path):
        self.path = path

    @abc.abstractmethod
    def read(self):
        pass  # Subclass must implement.

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, self.path)


class GIFReader(ImageReader):
    def read(self):
        """Read a GIF"""
        pass


class JPEGReader(ImageReader):
    def read(self):
        """Read a JPEG"""
        pass


class PNGReader(ImageReader):
    def read(self):
        """Read a PNG"""
        pass


# The ImageReader class is marked abstract, requiring subclasses to implement the read method. So far, so good.
# Now, when reading an image file, if its extension is ".gif", I want to use GIFReader.
# And if it is a JPEG image, I want to use JPEGReader. And so on.
#
# The logic is
# Analyze the file path name to get the extension,
# choose the correct reader class based on that,
# and finally create the appropriate reader object.


# Let’s define a little helper function:

def extension_of(path):
    position_of_last_dot = path.rfind('.')
    return path[position_of_last_dot + 1:]


# With these pieces, we can now define the core logic


# First version of get_image_reader()
def get_image_reader(path):
    image_type = extension_of(path)
    reader_class = None
    if image_type == 'gif':
        reader_class = GIFReader
    elif image_type == 'jpg':
        reader_class = JPEGReader
    elif image_type == 'png':
        reader_class = PNGReader
    if reader_class is None:
        print('Unknown extension: {}'.format(image_type))
    return reader_class(path)

# Refactor the above code while preserving the same functionality

"""
Imagine a simple Money class suitable for currencies which have dollars and cents
"""


class Money(object):
    def __init__(self, dollars, cents):
        self.dollars = dollars
        self.cents = cents


# Perhaps you’re modelling a giant jar of pennies Emptying the penny jar ...

total_pennies = 3274  # // is integer division
dollars = total_pennies // 100
cents = total_pennies % 100
total_cash = Money(dollars, cents)


# Same code written as a method

def money_from_pennies(total_cents):
    dollars = total_cents // 100
    cents = total_cents % 100
    return Money(dollars, cents)


# Imagine that, in the same code base, you also routinely need to parse a string like "$140.75".
# Here’s another function for that
# Another function, creating Money from a string amount.


import re


def money_from_string(amount):
    match = re.search(r'^\$(?P<dollars>\d+)\.(?P<cents>\d\d)$', amount)
    if match is None:
        raise ValueError('Invalid amount: {}'.format(amount))
    dollars = int(match.group('dollars'))
    cents = int(match.group('cents'))
    return Money(dollars, cents)


# These are effectively alternate constructors: callables we can use with different arguments, which are parsed and used
# to create the final object. But this approach has problems.
# First, it’s awkward to have them as separate functions, defined outside of the class.
# But much more importantly: what happens if you subclass Money? Suddenly
# money_from_string and money_from_pennies are worthless.The base Money class is hard-coded in the functions


class TipMoney(Money):
    pass

# tip = TipMoney.from_pennies(475)

# Create a Money class with multiple constructors. the Money class can be subclassed and if we later on change it's name
# your code should still work

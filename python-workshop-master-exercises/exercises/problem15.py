"""
Re-factor your answer in problem14 to fit these test-cases
Doing this will make it more shorter
The difference here is that we're passing a function as an argument
In problem14 we passed the Promotion object as an argument

joe = Customer('John Doe', 0)
ann = Customer('Ann Smith', 1100)
cart = [LineItem('banana', 4, .5),
         LineItem('apple', 10, 1.5),
         LineItem('watermellon', 5, 5.0)]
Order(joe, cart, fidelity_promo)
<Order total: 42.00 due: 42.00>

Order(ann, cart, fidelity_promo)
<Order total: 42.00 due: 39.90>
banana_cart = [LineItem('banana', 30, .5),
                LineItem('apple', 10, 1.5)]
Order(joe, banana_cart, bulk_item_promo)
<Order total: 30.00 due: 28.50>

long_order = [LineItem(str(item_code), 1, 1.0)
               for item_code in range(10)]

Order(joe, long_order, large_order_promo)
<Order total: 10.00 due: 9.30>

Order(joe, cart, large_order_promo)
<Order total: 42.00 due: 42.00>

"""
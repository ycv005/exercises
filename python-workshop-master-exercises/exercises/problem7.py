# In Python every class can have instance attributes.
# By default Python uses a dict to store an object’s instance attributes.
# This is really helpful as it allows setting arbitrary new attributes at runtime.

# However, for small classes with known attributes it might be a bottleneck.

# The dict wastes a lot of RAM. Python can’t just allocate a static amount of memory at object creation to store
# all the attributes. Therefore it sucks a lot of RAM if you create a lot of objects (thousands and millions).


# Imagine the class below

class MyClass(object):
    def __init__(self, name, identifier):
        self.name = name
        self.identifier = identifier
        self.set_up()
    # ...


# Which feature of python can help you reduce the RAM usage of this class when you create millions of objects?

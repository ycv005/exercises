"""
Imagine your team-mate wrote this code and told you the exception can be safely ignored
Most self-destructive code any python dev can write
Re-write this snippet to make it more reliable
"""


def extract_address(location_data):
    """
    Gives the address
    :param location_data:
    :return:
    """
    address = location_data.split(',')
    return address


try:
    extract_address(location_data)
except ValueError:
    pass
